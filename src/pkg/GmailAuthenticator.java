package pkg;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class GmailAuthenticator extends Authenticator {
	String user,pwd;
	
	public GmailAuthenticator(String u, String p){
		user=u;
		pwd=p;
	}
	
	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(user,pwd);
	}

}
