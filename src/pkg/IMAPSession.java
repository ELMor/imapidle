package pkg;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import com.sun.mail.imap.IMAPFolder;

public class IMAPSession {
	String imapUser,imapPassword;
	String imapSrv, imapProto;
	String smtpProto, smtpSrv, smtpUser, smtpPassword, smtpPort;
	boolean smtpTLS, smtpAuth;
	
	protected Session session;
	private Store store;
	private Hashtable<String, IMAPFolder> openFolders=new Hashtable<String, IMAPFolder>();
	IMAPFolder root=null;
	int mode=0;

	
	protected IMAPSession(boolean write,
			String imapUsr, String imapPwd, String imapSrv, String imapProt,
			String smtpUsr, String smtpPwd, String smtpSrv, String smtpProt, 
			String smtpPort, boolean smtpTLS, boolean smtpAuth){
		mode=write ? Folder.READ_WRITE : Folder.READ_ONLY;
		setIMAP(imapUsr, imapPwd, imapSrv, imapProt);
		setSMTP(smtpProt, smtpTLS, smtpSrv, smtpUsr, smtpPwd, smtpPort, smtpAuth);
		if(!logon()){
			throw new RuntimeException("Cannot connect to IMAP store");
		}
	}
	
	public void close(){
		for(Enumeration<IMAPFolder> fs=openFolders.elements();
			fs.hasMoreElements();){
			IMAPFolder f=fs.nextElement();
			if(f.isOpen())
				try {
					f.close(mode==Folder.READ_WRITE ? true : false);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
		}
		try {
			store.close();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	protected IMAPFolder open(String fid){
		IMAPFolder ret=openFolders.get(fid);
		if(ret==null){
			try {
				ret=(IMAPFolder)store.getFolder(fid);
				ret.open(mode);
				openFolders.put(fid, ret);
			} catch (MessagingException e) {
				e.printStackTrace();
				ret=null;
			}
		}
		return ret;
	}
	
	private void setIMAP(String use, String pas,String srv, String protocol){
		imapUser=use;
		imapPassword=pas;
		imapSrv = srv;
		//Puerde ser imap o imaps
		imapProto = (protocol==null ? "imap" : protocol);
	}

	private void setSMTP(String prot, boolean tls, String host, String usr, String pwd, String port, boolean auth){
		//Puede ser smtp o smtps al menos
		smtpProto=prot;
		smtpTLS=tls;
		smtpSrv=host;
		smtpUser=usr;
		smtpPassword=pwd;
		smtpPort=port;
		smtpAuth=auth;
	}
	
	private boolean logon() {
		Properties props=new Properties();
		if (session == null) {
			props.put("mail.user", imapUser);
			props.put("mail.store.protocol", imapProto);

			props.put("mail.transport.protocol", smtpProto);
			if(smtpTLS)
				props.put("mail.smtp.starttls.enable","true");
			props.put("mail.smtp.host", smtpSrv);
			props.put("mail.smtp.user", smtpUser);
			props.put("mail.smtp.from", imapUser);
			props.put("mail.smtp.password", smtpPassword);
			if(smtpPort!=null)
				props.put("mail.smtp.port", smtpPort);
			if(smtpAuth)
				props.put("mail.smtp.auth", "true");
			//props.put("mail.smtps.quitwait", "false");
			
			session = Session.getInstance(props, new GmailAuthenticator(imapUser,imapPassword));
		}
		try {
			store = session.getStore("imaps");
			store.connect(imapSrv, imapUser, imapPassword);
			root=(IMAPFolder)store.getDefaultFolder();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean Logoff(){
		try {
			for(Enumeration<IMAPFolder> f=openFolders.elements();
			    f.hasMoreElements();){
				f.nextElement().close(true);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		} finally {
			try {
				store.close();
			}catch(MessagingException me){
				
			}
		}
		return true;
	}

}
